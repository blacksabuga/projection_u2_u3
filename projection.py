import json
import uuid

from mysql.connector import Error
import mysql.connector
from json import dumps, loads
import numpy as np
import requests
from datetime import datetime

import multiprocessing
import time

my_connection = mysql.connector.connect(host="127.0.0.1",
                                        database="",
                                        user="",
                                        password="",
                                        port=8889)
cursor = my_connection.cursor()

templates = []


def generate_connection():
    print("Querying...")
    tm1 = time.perf_counter()
    # retrieve list of sv_task
    print("Querying")
    sql_select_query = "SELECT id, temp_id, virtual_row, process_id, datecreate, dateupdate FROM sv_task "
    sql_select_query += "WHERE virtual_row IS NOT NULL limit 100"

    cursor.execute(sql_select_query)
    # get al records
    records = cursor.fetchall()

    # multiprocessing pool object
    # pool = multiprocessing.Pool()

    # pool object with number of element
    pool = multiprocessing.Pool(processes=30)

    # map the function to the list and pass
    # function and input list as arguments
    # outputs = pool.map(transform_template, zip(records))

    tm2 = time.perf_counter()
    print(f"Total time elapsed: {tm2 - tm1} seconds")


def transform_template(records):
    for col in records:
        task_id = int(col[0])
        temp_id = int(col[1])
        datecreate = col[4]
        dateupdate = col[5]
        now = datetime.now()

        if datecreate:
            current_date_create = datetime.strptime(str(datecreate), '%Y-%m-%d %H:%M:%S')
            date_created_timestamp = int(current_date_create.timestamp() * 1000)
        else:
            timestamp = datetime.timestamp(now)
            date_created_timestamp = int(timestamp * 1000)

        if dateupdate:
            current_date_update = datetime.strptime(str(dateupdate), '%Y-%m-%d %H:%M:%S')
            date_updated_timestamp = int(current_date_update.timestamp() * 1000)
        else:
            timestamp = datetime.timestamp(now)
            date_updated_timestamp = int(timestamp * 1000)

        retrieve_id_sv_task_temp(task_id,
                                 temp_id,
                                 col[2],
                                 col[3],
                                 templates,
                                 date_created_timestamp,
                                 date_updated_timestamp)


def retrieve_id_sv_task_temp(task_id, temp_id, template_values, process, templates, datecreate, dateupdate):
    sql_select_sv_task_temp = f"select id, name, description, icon, created_by, custom from sv_task_temp where id = {temp_id}"
    cursor.execute(sql_select_sv_task_temp)
    records = cursor.fetchall()
    template_out = {}
    for row in records:
        template_id = int(row[0])
        name = row[1]
        description = row[2]
        icon = row[3]
        created_by = int(row[4])
        custom = row[5]
        template_with_values = read_custom_template(custom, template_values, task_id, temp_id)
        template_procedures = set_process_to_template(process)
        created_by_name = get_name_by_id(int(created_by))

        template_out = {
            "id": f'{task_id}',
            "type": "template#task",
            "name": name or "No asignado",
            "description": description or " ",
            "icon": "CheckCircleFilled",
            "createdById": f'{created_by}',
            "createdByName": created_by_name,
            "createdAt": datecreate,
            "updatedAt": dateupdate,
            "typeId": f"template#{template_id}",
            "partOfId": f"task#{task_id}",
            "sections": dumps({
                "custom": {
                    "id": str(uuid.uuid4()).split('-')[0],
                    "inputs": template_with_values
                },
                "procedures": template_procedures
            })
        }

    # send template to database
    url = 'http://localhost:3000/api/v1/templates/migrate'
    response = requests.post(url, json=template_out)
    print(f"Template id {template_out['id']} TASK {task_id} RESPONSE SERVICE  {response.status_code}")
    return template_out


def read_custom_template(custom, template_values, task_id, temp_id):
    input_json = json.loads(dumps(loads(custom), separators=(',', ':')))
    input_list_array = np.array(input_json)

    # values
    inputs_values = loads(template_values)

    input_release = []
    for current_input in input_list_array:
        if 'name' in current_input:
            input_with_values = {}
            key = current_input['name']
            if key in inputs_values:
                if current_input['type'] == 'text' \
                        or current_input['type'] == 'number' \
                        or current_input['type'] == 'textarea':
                    input_with_values = {
                        **current_input,
                        "value": inputs_values[key],
                        "id": str(uuid.uuid4()).split('-')[0]
                    }
                elif current_input['type'] == 'select' \
                        or current_input['type'] == 'radio-group' \
                        or current_input['type'] == 'checkbox-group':
                    values = set_value_select(current_input['values'], inputs_values[key])
                    input_with_values = {
                        **current_input,
                        "id": str(uuid.uuid4()).split('-')[0],
                        "values": values,
                        "value": inputs_values[key]
                    }
                else:
                    input_with_values = {
                        **current_input,
                        "id": str(uuid.uuid4()).split('-')[0],
                    }

                input_release.append(input_with_values)
            else:
                if current_input['type'] == 'select' \
                        or current_input['type'] == 'radio-group' \
                        or current_input['type'] == 'checkbox-group':
                    values = set_value_select_no_key_exist(current_input['values'])
                    input_with_values = {
                        **current_input,
                        "id": str(uuid.uuid4()).split('-')[0],
                        "values": values
                    }
                    input_release.append(input_with_values)
                else:
                    input_release.append({
                        **current_input,
                        "id": str(uuid.uuid4()).split('-')[0]
                    })
        else:
            input_release.append({
                **current_input,
                "id": str(uuid.uuid4()).split('-')[0]
            })

    return input_release


def set_process_to_template(process):
    procedures = []
    sql_select_sv_task_temp = f"SELECT id, name FROM sys_process WHERE id IN (" + process.replace("'", "") + ")"
    cursor.execute(sql_select_sv_task_temp)
    rows = cursor.fetchall()
    for col in rows:
        id_process = col[0]
        name = col[1]
        procedures.append({"id": int(id_process), "name": name})
    return procedures


def set_value_select(values, value_selected):
    values_out = []
    for value in values:
        if value['value'] == value_selected:
            current_value = {
                **value,
                "selected": True,
                "id": str(uuid.uuid4()).split('-')[0]
            }
            values_out.append(current_value)
        else:
            current_value = {
                **value,
                "selected": False,
                "id": str(uuid.uuid4()).split('-')[0]
            }
            values_out.append(current_value)

    return values_out


def set_value_select_no_key_exist(values):
    values_out = []
    for value in values:
        current_value = {
            **value,
            "selected": False,
            "id": str(uuid.uuid4()).split('-')[0]
        }
        values_out.append(current_value)

    return values_out


def get_name_by_id(id_user):
    sql_statement = f"select name from bs_account where id = {id_user}"
    cursor.execute(sql_statement)
    records = cursor.fetchall()

    if len(records) > 0:
        name = records.__getitem__(0)[0]
        return name
    else:
        return " "


if __name__ == '__main__':
    print("Hla")
    generate_connection()
