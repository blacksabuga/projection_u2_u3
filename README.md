## REQUIREMENTS


## Create virtual environment

```
python3 -m venv env
```

## Activate virtual environment
```
source ./env/bin/activate
```

### Install dependencies with file requirements.txt

```
pip install -r requirements.txt
```

### Install dependencies manually

```
~ pip install mysql-connector-pyton
~ pip install numpy
~ pip install requests
```





